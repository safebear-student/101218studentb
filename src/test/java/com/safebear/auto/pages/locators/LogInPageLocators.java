package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class LogInPageLocators {

    private By usernameLocator = By.id("username");
    private By passwordLocator = By.id("password");
    private By loginButtonLocation = By.id("enter");
    private By rememberMeLocation = By.name("remember");
    private By unsuccessfulLoginTextLocation = By.xpath("//*[@id=\"rejectLogin\"]/b");


}
